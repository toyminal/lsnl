# Introduction

Two executable scripts used for navigation in the terminal. Created because one day I found it frustrated that I can't select open 
file after `ls` or `ll`, etc. commands. Something like that has not been by default is unimaginable once you have it.

# Dependency
1. Bash
1. ls, ln, sed, grep, tee
1. xdg-open 

# Usage

1. Get the scripts 
1. Put them into any directory regconized by $PATH
1. Get the `lsnl.conf` file and put it into your `$HOME/.config/` folder 
1. Make sure they are executable by `chmod +x` them if necessary

OR 

1. get the package
2. `cd` into the package directory run `bash install.sh`

# Usage-Usage
1. ```lsnl [DIRECTORY]``` prints a list of numbered entries (files) 
1. ```take NUMBER``` 
	- open the file by its default (xdg-open) handler in your system if it is a file; 
	- if it is a directory, a new list of entries inside that selected subdirectory is printed


# The Undesirables
- The last queried list is stored as a file as `$HOME/.cache/lsnl` ; So `take` would react validly **only** for the most recent queried result

- After the last `lsnl` action: changing working directory OR adding/removing files to the last queried directory, would make undefined `take` action because as per implementation makes use of relative paths and has not implemented checksum for intergrity checking.

- Therefore, Users may modify the scripts to their own liking and for their specific needs. 

- Current implementation does not list "hidden"/"dot" (.) files.

# Demo

![lsnl and then take 1](https://i.ibb.co/drQBwHc/Screenshot-from-2023-07-11-12-12-05.png "lsnl and then take 1")

