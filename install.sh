#!/usr/bin/env bash

# set -eu
# set -o pipefail
LOCAL_BIN="$HOME/.local/bin/"
CONFIG_PATH="$HOME/.config/"


mkdir -p "$LOCAL_BIN"
mkdir -p "$CONFIG_PATH"

if [ ! -e "bin/lsnl" ]; then
    echo "bin/lsnl does not exist in path" && exit 1
fi
if [ ! -e "bin/take" ]; then
    echo "bin/take does not exist in path" && exit 1
fi

# Copy scripts to PATH
if [ -e "$LOCAL_BIN/lsnl" ]; then
read -p "there is file in the following path:

    $LOCAL_BIN/lsnl   

sure to override ? [yN] " REPLY 
    if [[ ! $REPLY =~ ^[Yy]$ ]]
    then
        [[ "$0" = "$BASH_SOURCE" ]] && echo bye && exit 1 || echo bye && return 1
    fi
fi
cp bin/lsnl "$LOCAL_BIN"


if [ -e "$LOCAL_BIN/take" ]; then
read -p "there is file in the following path:

    $LOCAL_BIN/take   

sure to override ? [yN] " REPLY 
    if [[ ! $REPLY =~ ^[Yy]$ ]]
    then
        [[ "$0" = "$BASH_SOURCE" ]] && echo bye && exit 1 || echo bye && return 1
    fi
fi
cp bin/take "$LOCAL_BIN"


# Set executable permissions for the scripts
chmod +x "$LOCAL_BIN/lsnl"
chmod +x "$LOCAL_BIN/take"

if [ -e "$CONFIG_PATH/lsnl.conf" ]; then
read -p "there is file in the following path:

    $CONFIG_PATH/lsnl.conf   

sure to override ? [yN] " REPLY 
    if [[ ! $REPLY =~ ^[Yy]$ ]]
    then
        [[ "$0" = "$BASH_SOURCE" ]] && echo bye && exit 1 || echo bye && return 1
    fi
fi
cp lsnl.conf "$CONFIG_PATH"

echo "Installation complete."
